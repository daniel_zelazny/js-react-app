import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route } from 'react-router-dom'

const App = () => {
    return (
        <BrowserRouter>
            <div className={'section-wrapper'}>
                This is some react app
            </div>
        </BrowserRouter>
    );
};
export default App

ReactDOM.render(<App />, document.getElementById('app'))